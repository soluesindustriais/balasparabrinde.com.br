<div class="col-12 pt-4">
    <div class="jumbotron py-1">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 py-lg-4 py-3 text-center"><a
                    href="<?=$url?>imagens/balas-personalizadas/balas-personalizadas-01.jpg" class="lightbox"
                    title="<?=$h1?>"><img src="<?=$url?>imagens/balas-personalizadas/thumbs/balas-personalizadas-01.jpg"
                        class="img-thumbnail mb-1 lazy" alt="<?=$h1?>" title="<?=$h1?>" /></a><strong>Imagem ilustrativa
                    de <?=$h1?></strong></div>
            <div class="col-lg-6 col-md-6 col-sm-12 py-lg-4 py-3 text-center"><a
                    href="<?=$url?>imagens/balas-personalizadas/balas-personalizadas-02.jpg" title="<?=$h1?>" class="lightbox"
                    target="_blank"><img src="<?=$url?>imagens/balas-personalizadas/thumbs/balas-personalizadas-02.jpg"
                        class="img-thumbnail mb-1 lazy" alt="<?=$h1?>" title="<?=$h1?>" /></a><strong>Imagem ilustrativa
                    de <?=$h1?></strong></div>
        </div>
    </div>
</div>
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ItemList",
    "itemListElement": [{
            "@type": "ImageObject",
            "author": "Soluções Industriais",
            "contentUrl": "<?= $url ?>imagens/balas-personalizadas/thumbs/balas-personalizadas-01.jpg",
            "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto.",
            "name": "<?= $h1 ?> modelo 01",
            "uploadDate": "2024-03-05"
        },
        {
            "@type": "ImageObject",
            "author": "Soluções Industriais",
            "contentUrl": "<?= $url ?>imagens/balas-personalizadas/thumbs/balas-personalizadas-02.jpg",
            "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto.",
            "name": "<?= $h1 ?> modelo 02",
            "uploadDate": "2024-03-05"
        }
    ]
}
</script>