
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<div class="container p-0 my-3"><div class="row no-gutters justify-content-center py-3"><div class="col-lg-12 pt-2 mt-2 text-center"><div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag"><div class="col-12 py-2"><div class="owl-stage-outer"><div class="owl-stage"> <?php $lista = array('Bala Brinde','Bala Com Furo Para Empresa','Bala Com Furo Personalizada','Bala Com Furo Personalizada Para Clientes','Bala De Goma Americana Personalizada','Bala De Goma Personalizada','Bala Personalizada Chapada','Bala Personalizada Dia Da Mulher','Bala Personalizada Laminada','Bala Personalizada Laminada Preço','Bala Personalizada Metalizada','Bala Personalizada Metalizada Preço','Bala Personalizada Natal','Bala Personalizada Prata','Bala Personalizada Prateada','Bala Personalizada Transparente','Bala Personalizada Transparente Preço','Bala Premium Personalizada','Bala Premium Personalizada Preço','Balas Brindes Personalizados','Balas Coloridas Personalizadas','Balas Coloridas Personalizadas Preço','Balas Com Embalagens Personalizadas','Balas Com Logo Da Empresa','Balas Com Logomarca','Balas Com Logomarca Para Brinde','Balas Com Logomarca Para Empresa','Balas Com Logomarca Personalizadas','Balas Com Logotipo De Empresa','Balas Customizadas','Balas De Natal Personalizadas','Balas Duras Personalizadas','Balas Duras Personalizadas Preço','Balas Embalagens Metalizadas','Balas Frutmel','Balas Frutmel Personalizadas','Balas Frutmel Preço','Balas Mastigáveis Personalizadas','Balas Para Casamento','Balas Para Empresa','Balas Para Empresa Preço','Balas Para Festa','Balas Personalizadas','Balas Personalizadas Ano Novo','Balas Personalizadas Casamento','Balas Personalizadas Com Logo','Balas Personalizadas De Brinde','Balas Personalizadas Dia Das Mães','Balas Personalizadas Dia Dos Namorados','Balas Personalizadas Em Bh','Balas Personalizadas Em Fortaleza','Balas Personalizadas Em Recife','Balas Personalizadas Outubro Rosa','Balas Personalizadas Para Clientes','Balas Personalizadas Para Empresa','Balas Personalizadas Para Loja','Balas Personalizadas Páscoa','Balas Personalizadas Porto Alegre','Balas Personalizadas Preço','Balas Personalizadas Ribeirão Preto','Balas Personalizadas Rj','Balas Personalizadas Sp','Balas Personalizadas Valor','Balas Promocionais','Balas Promocionais Para Empresa','Balas Promocionais Personalizadas','Balas Soft Personalizadas','Balas Soft Personalizadas Para Brindes','Balas Transparentes Personalizadas','Balinha De Brinde','Balinha Personalizada','Balinha Personalizada Dia Das Mães','Balinha Personalizada Para Loja','Balinhas Para Empresas','Balinhas Personalizadas Para Clientes','Balinhas Personalizadas Para Empresas','Balinhas Personalizadas Valor','Comprar Bala Personalizada Laminada','Comprar Bala Personalizada Metalizada','Comprar Balas Coloridas Personalizadas','Comprar Balas Com Logomarca','Comprar Balas Duras Personalizadas','Comprar Balas Para Empresa','Comprar Balas Personalizadas','Comprar Balas Personalizadas Transparentes','Comprar Balas Transparentes Personalizadas','Comprar Goma Americana Personalizada','Embalagem Chapada Para Balas Personalizadas','Embalagem De Balinha Personalizada','Embalagem Flow Pack Personalizada Para Bala','Embalagem Laminada Para Balas Personalizadas','Embalagem Metalizada Para Balas Personalizadas','Embalagem Transparente Para Balas Personalizadas','Embalagens Para Balas Personalizadas','Embalagens Personalizadas Para Balas','Embalagens Plasticas Para Balas','Fábrica De Balas Personalizadas','Goma Americana Com Embalagem Personalizada','Goma Americana Personalizada','Goma Americana Personalizada Preço','Valor Balas Personalizadas'); shuffle($lista); for($i=1;$i<13;$i++){ ?> <div class="owl-item"><div class="card blog rounded border-0 shadow overflow-hidden"> <div class="position-relative border-intro"><a class="lightbox" href="<?=$url;?>imagens/balas-personalizadas/balas-personalizadas-<?=$i?>.jpg" title="<?=$lista[$i]?>"><img src="<?=$url;?>imagens/balas-personalizadas/thumbs/balas-personalizadas-<?=$i?>.jpg" alt="<?=$lista[$i]?>" title="<?=$lista[$i]?>"/><div class="overlay rounded-top bg-dark"></div><div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?=$lista[$i]?></small></div> </div></div></a></div><?php } ?></div></div></div></div></div></div></div>
<?php
$folder = "balas-personalizadas";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/balas-personalizadas/balas-personalizadas-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-05"
];

?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/balas-personalizadas/balas-personalizadas-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/balas-personalizadas/thumbs/balas-personalizadas-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>