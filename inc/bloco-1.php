<? include('inc/vetDestaque.php'); ?>
<section class="section pt-5">
    <div class="container mt-0 pt-2">


        <div class="section pb-3 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title mb-4 pb-2">
                            <h2 class="title mb-4">Conheça nossos produtos</h2>
                            <p class="text-muted para-desc mx-auto mb-0">Receba cotação de
                                <? 
                  $i = 0; 
                  $numCategorias = count($categorias->getCategorias())-1; 
                  foreach($categorias->getCategorias() as $categoria):
                     $categoriaSemAcento = $trata->trataAcentos($categoria);
                     $categoriaSemHifen = $trata->retiraHifen($categoria);
                  ?>

                                <? if ( $i < $numCategorias ): ?>
                                <a href="<?=$categoriaSemAcento."-categoria"; ?>"
                                    class="text-primary font-weight-bold cor-fonte"><?= $categoriaSemHifen; ?>, </a>
                                <? else: ?>
                                <a href="<?=$categoriaSemAcento."-categoria"; ?>"
                                    class="text-primary font-weight-bold cor-fonte"><?= $categoriaSemHifen; ?></a>
                                <? endif; ?>

                                <? $i++; endforeach; ?>
                                e muito mais.
                            </p>
                        </div>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
                <!-- Partners End -->
                <div class="row">
                    <div class="col-12 mt-4 pt-2">
                        <div id="customer-testi" class="owl-carousel owl-theme owl-loaded owl-drag">
                            <div class="owl-stage-outer">
                                <div class="owl-stage">
                                    <? foreach($vetDestaque as $palavras => $palavra): ?>
                                    <div style="height: 280px; max-height: 100%;"
                                        class="owl-item overflow-hidden position-relative products-carousel-home">
                                        <a href="<?= $trata->trataAcentos(($palavra['palavra'])); ?>">
                                            <div
                                                class="card customer-testi border-0 text-center position-relative bg-transparent">
                                                <img class="w-100 mw-100 position-absolute absolute-top absolute-left"
                                                    src="imagens/home/destaque/<?= $trata->trataAcentos(($palavra['palavra'])); ?>.jpg"
                                                    alt="<?=$trata->retiraHifen($palavra['palavra']);?>">
                                                <div class="card-body position-relative overflow-hidden bg-transparent">
                                                    <!--  <p class="d-none text-dark mt-1"><?=$palavra['texto'];?></p> -->
                                                    <h3 class="text-primary text-center mt-3">
                                                        <?=$trata->retiraHifen($palavra['palavra']);?></h3>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- CARROSSEL COM LOGO INIT
      <div class="row pb-5">
            <div class="container mb-5 py-5">

            <div id="customer-test" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">

                     <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/instalacao-e-montagem-industrial/instalacao-e-montagem-industrial-01.jpg" class="avatar avatar-medium w-100" alt="instalacao-e-montagem">
                           </div>
                        </div>
                     </div>
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/manutencao-predial/manutencao-predial-01.jpg" class="avatar avatar-medium w-100" alt="manutencao-predial">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/painel-e-quadro-eletrico/painel-e-quadro-eletrico-02.jpg" class="avatar avatar-medium w-100" alt="painel-e-quadro-eletrico">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/servicos-para-camaras-frias/servicos-para-camaras-frias-02.jpg" class="avatar avatar-medium w-100" alt="servicos-para-camaras-frias">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/servicos-spda-para-raios/servicos-spda-para-raios-4.jpg" class="avatar avatar-medium w-100" alt="">
                           </div>
                        </div>
                     </div>                      
                  </div>
               </div>
            </div>
                  </div>
   </div> 
      CARROSSEL COM LOGO END-->



                <div class="row">

                    <!--end container-->
                    <div class="container mt-100 mt-60">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <img src="imagens/<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>/<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>-01.jpg"
                                    class="img-fluid shadow rounded mw-100"
                                    alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0]));?>">
                            </div>
                            <!--end col-->
                            <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                <div class="section-title ml-lg-5">
                                    <h4 class="title mb-4">
                                        <?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0])); ?>
                                    </h4>
                                    <p class="text-muted">Balas personalizadas aumentam a credibilidade das festas,
                                        deixam sua marca com maior destaque na indústria e colocam sua cara no mundo dos
                                        doces de maneira simples e fácil!</p>
                                    <a href="<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>-categoria"
                                        class="mt-3 text-primary">Saiba Mais </a>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </div>
                    <!--end container-->
                    <?php if(isset($categorias->getCategorias()[1])){ ?>
                    <div class="container mt-100 mt-60">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                <div class="section-title mr-lg-5">
                                    <h4 class="title mb-4">
                                        <?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1])); ?>
                                    </h4>
                                    <p class="text-muted">Doces comuns já são algo do passado, doces personalizados
                                        deixam a festa ainda mais interessante e temática além de terem excelência no
                                        acabamento e sabor.</p>
                                    <a href="<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-categoria"
                                        class="mt-3 text-primary">Saiba Mais</a>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                <img src="imagens/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-01.jpg"
                                    class="img-fluid shadow rounded mw-100"
                                    alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1]));?>">
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </div>

                    <div class="container mt-100 mt-60">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <img src="imagens/<?= $trata->trataAcentos($categorias->getCategorias()[2]); ?>/<?= $trata->trataAcentos($categorias->getCategorias()[2]); ?>-01.jpg"
                                    class="img-fluid shadow rounded mw-100"
                                    alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[2]));?>">
                            </div>
                            <!--end col-->
                            <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                <div class="section-title ml-lg-5">
                                    <h4 class="title mb-4">
                                        <?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[2])); ?>
                                    </h4>
                                    <p class="text-muted">É fundamental que o cliente conte com a empresa que tenha como
                                        principal missão oferecer o melhor acabamento e qaulidade, pirulitos
                                        personalizados dão á marca maior credito e são ótimos para campanhas e eventos.
                                    </p>
                                    <a href="<?= $trata->trataAcentos($categorias->getCategorias()[2]); ?>-categoria"
                                        class="mt-3 text-primary">Saiba Mais </a>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </div>


                    <!--end container-->
                    <?php } ?>