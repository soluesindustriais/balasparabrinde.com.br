
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<div class="container p-0 my-3"><div class="row no-gutters justify-content-center py-3"><div class="col-lg-12 pt-2 mt-2 text-center"><div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag"><div class="col-12 py-2"><div class="owl-stage-outer"><div class="owl-stage"> <?php $lista = array('Embalagem Personalizada Para Amendoim','Embalagem Personalizada Para Doces','Amendoim Japones Personalizado','Amendoim Japones Personalizado Para Brindes','Amendoim Japones Personalizado Para Empresa','Amendoim Japones Personalizado Para Eventos','Amendoim Japones Personalizado Preço','Amendoim Para Brinde Em Evento','Amendoim Personalizado','Amendoim Personalizado Para Brinde','Amendoim Personalizado Para Empresa','Amendoim Personalizado Para Eventos','Amendoim Personalizado Preço','Comprar Amendoim Japones Personalizado','Comprar Amendoim Personalizado','Doce De Amendoim Para Brinde Personalizado','Doce De Amendoim Personalizado Brinde','Doce De Amendoim Personalizado Para Empresa','Doces Customizados','Doces Para Festa De Formatura','Doces Para Festa Personalizados','Doces Personalizados','Doces Personalizados Formatura','Doces Personalizados Para Casamento','Doces Personalizados Preço','Doces Personalizados Sp','Doces Personalizados Valor','Doces Personalizados Zona Leste','Doces Personalizados Zona Norte Sp','Docinhos Personalizados','Docinhos Personalizados Preço','Kit Doces Personalizados','Mini Pé De Moleque Personalizado','Mini Pé De Moleque Personalizado Comprar','Mini Pé De Moleque Personalizado Para Brindes','Mini Pé De Moleque Personalizado Preço','Pastilhas De Chocolate Personalizadas','Pé De Moleque Com Embalagem Personalizada','Pé De Moleque Para Brindes','Pé De Moleque Personalizado','Pé De Moleque Personalizado Brinde','Pé De Moleque Personalizado Para Empresa','Pé De Moleque Personalizado Preço','Preço De Doces Personalizados','Valor De Doces Personalizados'); shuffle($lista); for($i=1;$i<13;$i++){ ?> <div class="owl-item"><div class="card blog rounded border-0 shadow overflow-hidden"> <div class="position-relative border-intro"><a class="lightbox" href="<?=$url;?>imagens/doces-personalizados/doces-personalizados-<?=$i?>.jpg" title="<?=$lista[$i]?>"><img src="<?=$url;?>imagens/doces-personalizados/thumbs/doces-personalizados-<?=$i?>.jpg" alt="<?=$lista[$i]?>" title="<?=$lista[$i]?>"/><div class="overlay rounded-top bg-dark"></div><div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?=$lista[$i]?></small></div> </div></div></a></div><?php } ?></div></div></div></div></div></div></div>
<?php
$folder = "doces-personalizados";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/doces-personalizados/doces-personalizados-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-05"
];

?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/doces-personalizados/doces-personalizados-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/doces-personalizados/thumbs/doces-personalizados-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>