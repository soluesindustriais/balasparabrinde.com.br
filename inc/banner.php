        <div class="cd-hero">
            <ul class="cd-hero-slider autoplay">
                <li class="selected">
                    <div class="cd-full-width">
                        <h2>Doces Personalizados</h2>
                        <p>Doces comuns já são algo do passado, doces personalizados <span>deixam a festa ainda mais
                                interessante e temática </span> além de terem excelência no acabamento e sabor.</p>
                        <a href="<?= $url ?>doces-personalizados" class="cd-btn">Saiba mais</a>
                    </div>

                </li>
                <li>
                    <div class="cd-full-width">
                        <h2>Balas Personalizadas</h2>
                        <p>Balas personalizadas aumentam a credibilidade das festas, deixam sua marca com maior destaque
                            na indústria e colocam sua cara no mundo dos doces de maneira simples e fácil!</p>
                        <a href="<?= $url ?>balas-personalizadas" class="cd-btn">Saiba mais</a>
                    </div>

                </li>
                <li>
                    <div class="cd-full-width">
                        <h2>Brindes Corporativos</h2>
                        <p>É fundamental que o cliente conte com a empresa que tenha como principal missão oferecer o
                            melhor acabamento e qualidade, pirulitos personalizados dão á marca maior credito e são
                            ótimos para campanhas e eventos.</p>
                        <a href="<?= $url ?>brindes-corporativos" class="cd-btn">Saiba mais</a>
                    </div>

                </li>
            </ul>
            <div class="cd-slider-nav">
                <nav>
                    <span class="cd-marker item-1"></span>
                    <ul class="bannder-slider">
                        <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                        <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                        <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                    </ul>
                </nav>
            </div>

        </div>



        <!-- Hero End -->