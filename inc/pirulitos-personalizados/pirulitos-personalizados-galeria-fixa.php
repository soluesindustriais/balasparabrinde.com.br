
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<div class="container p-0 my-3"><div class="row no-gutters justify-content-center py-3"><div class="col-lg-12 pt-2 mt-2 text-center"><div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag"><div class="col-12 py-2"><div class="owl-stage-outer"><div class="owl-stage"> <?php $lista = array('Balas E Pirulitos Personalizados','Embalagem De Pirulito Personalizado','Embalagem Flow Pack Personalizada Para Pirulito','Empresa De Pirulitos Personalizados','Fruit Pop Com Embalagem Personalizada','Pirulito Brinde Para Cliente','Pirulito Colorido Personalizado Para Brinde','Pirulito Com Embalagem Personalizada','Pirulito De Chocolate Personalizado','Pirulito De Chocolate Personalizado Com Nome','Pirulito Moeda Personalizado','Pirulito Moeda Personalizado Para Empresa','Pirulito Personalizado Com Nome','Pirulito Personalizado Empresa','Pirulito Personalizado Laminado','Pirulito Personalizado Para Cliente','Pirulito Promocional Personalizado','Pirulito Redondo Personalizado','Pirulitos Personalizados','Preço De Pirulito De Chocolate Personalizado','pirulito de biscoito personalizado','pirulito de chocolate personalizado preço','pirulito quadrado personalizado','pirulitos personalizados para festa infantil'); shuffle($lista); for($i=1;$i<13;$i++){ ?> <div class="owl-item"><div class="card blog rounded border-0 shadow overflow-hidden"> <div class="position-relative border-intro"><a class="lightbox" href="<?=$url;?>imagens/pirulitos-personalizados/pirulitos-personalizados-<?=$i?>.jpg" title="<?=$lista[$i]?>"><img src="<?=$url;?>imagens/pirulitos-personalizados/thumbs/pirulitos-personalizados-<?=$i?>.jpg" alt="<?=$lista[$i]?>" title="<?=$lista[$i]?>"/><div class="overlay rounded-top bg-dark"></div><div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?=$lista[$i]?></small></div> </div></div></a></div><?php } ?></div></div></div></div></div></div></div>
<?php
$folder = "pirulitos-personalizados";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/pirulitos-personalizados/pirulitos-personalizados-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-05"
];

?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/pirulitos-personalizados/pirulitos-personalizados-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/pirulitos-personalizados/thumbs/pirulitos-personalizados-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>