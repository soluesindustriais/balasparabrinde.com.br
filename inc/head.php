<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/Thing" lang="pt-br">

<head>
    <meta charset="utf-8">
    <? include('inc/geral.php'); ?>

    <style>
        <? 
        include "css/style.css";
        include "css/owl.carousel.min.css";
        include "css/fancybox.css";
        include "css/bootstrap.min.css";
        include "css/jquery.fancybox.min.css";
        include "css/font-awesome.css";
        ?>
        </style>
    <script src="js/jquery-3.4.1.min.js"></script>

    <!-- <script src="https://code.jquery.com/jquery-3.7.0.min.js"
        integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script> -->
    <link async rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>