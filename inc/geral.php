<?

ini_set('display_errors', 0); error_reporting(E_ERROR | E_WARNING | E_PARSE); //Debug

$nomeSite			= 'Balas Para Brinde';
$slogan				= 'Soluções industriais';
$dir  = pathinfo($_SERVER['SCRIPT_NAME']);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/";  }
else { $url = $http."://".$host.$dir["dirname"]."/";  }

if ($_SERVER['HTTP_HOST'] != "localhost") $idAnalytics = "UA-119867186-34";

//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = strpos($urlhtaccess, 'https://www.') === false ? 'https://' : 'https://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess,'/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');

$emailContato		= 'growth.solucoesindustriais@gmail.com';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
//$idAnalytics		= 'UA-119867186-34';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
$tagsanalytic = ["G-F8J3T4CCFM","G-D23WW3S4NC"];

$idCliente = "";
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
$creditos = 'Soluções Industriais'; 


?>

<!-- CONFIGURAÇÃO SCHEMAS -->



<!--  META-TAGS -->

 	<meta name="csrf-token" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title><?=$h1.'  -  '.$nomeSite?></title>
	<base href="<?=$url?>">
	<meta name="description" content="<?=$desc?>">
	<meta name="keywords" content="<?=$key?>">
	<meta name="ICBM" content="-GEO-POSITION">
	<meta name="geo.position" content="GEO-POSITION">
	<meta name="geo.placename" content="SÃO PAULO-SP">
	<meta name="geo.region" content="SP-BR">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?=$url.$urlPagina?>">
	<meta name="author" content="<?=$nomeSite?>">	
	<link rel="shortcut icon" href="<?=$url?>/imagens/logo/favicon.png">
	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?=$nomeSite.' - '.$slogan?>">
	<meta property="og:type" content="article">
	<meta property="og:url" content="<?=$url.$urlPagina?>">
	<meta property="og:description" content="<?=$desc?>">
	<meta property="og:site_name" content="<?=$nomeSite?>">



<?

include('inc/vetCategorias.php');
include('inc/classes/trataString.class.php');
include('inc/classes/criarCategoria.class.php');
$trata = new Trata();
$categorias = new Categoria();
$categorias->setCategorias($vetCategorias);

//Função para gerar os arquivos *-categoria.php de cada categoria na raiz e gerar os folders de cada categoria na inc/
//Comentar a mesma ao fazer o deploy
$categorias->criarCategoria();
  

//Breadcrumbs
// 1 NIVEL
$caminho ='
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home   </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="2" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	  ';

$caminho2 ='
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home   </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="3" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	';


// 3 NIVEL
//Método para criar as breadcrumbs relativas às categorias
$categorias->createBreadcrumb();
include( 'inc/breadcrumb.php' ); 

?>

